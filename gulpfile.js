/**
 * @file
 * Gulp implementation.
 *
 * <code>
 * npm install --save gulp-sass gulp-autoprefixer gulp-concat gulp-imagemin \
 *   browser-sync gulp-cache gulp-notify gulp-rename
 * </code>
 */

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssbeautify = require('gulp-cssbeautify'),
    prefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

// Default Task.
gulp.task('default', ['css', 'js', 'watch']);

// Tasks.
// Compile sass.
gulp.task('css', function () {
  gulp.src('assets/sass/**/*.scss')
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(sass({ errLogToConsole: true }))
    .pipe(prefix('last 2 versions'))
    .pipe(cssbeautify({ indent: '  ' }))
    .pipe(gulp.dest('assets/css/'))
    .pipe(notify({ message: 'CSS task complete' }))
});

// Concatenate JS files.
gulp.task('js', function () {
  gulp.src('assets/js/src/*.js')
    .pipe(concat('bundle.js'))
    .pipe(notify({ message: 'JS task complete' }))
});

// Compress images.
gulp.task('images', function () {
  gulp.src('images/*.{gif,jpg,png}')
    .pipe(cache(imagemin({
      optimizationLevel: 4,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('images/'))
    .pipe(notify({ message: 'Images task complete' }))
});

// Reload browser.
gulp.task('reload', function () {
  browserSync.reload();
});

// Prepare Browser-sync.
gulp.task('browser-sync', function () {
  browserSync.init(['assets/css/src/**/*.scss', 'js/*.js'], {
    // proxy: 'your_dev_site.url'.
    server: { baseDir: './' }
  });
});

// Watch files for changes.
gulp.task('watch', function () {
  gulp.watch('assets/sass/**/*.scss', ['css']);
  gulp.watch('assets/js/*.js', ['js']);
  gulp.watch('assets/images/*.{gif,jpg,png}' , ['images']);
});
