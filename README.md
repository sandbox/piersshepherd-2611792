CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Maintainers


 INTRODUCTION
------------

Give your Drupal 8 site a beautiful, responsive admin theme.

 * Follows Google's material design guidelines
 * Easy to install
 * Themed toolbar with Atlas toolbar module
 * Responsive
 * Damn good looking, modern and functional design.


RECOMMENDED MODULES
-------------------

 * Atlas theme toolbar (https://www.drupal.org/sandbox/piersshepherd/2612096):
   When enabled, provides theming for the admin toolbar.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. See:
   https://www.drupal.org/getting-started/install-contrib/themes
   for further information.

 * Download.
 * Unzip.
 * Place inside the 'themes' directory. eg: themes/atlas/.
 * Install theme
 * Set the site admin theme to Atlas


MAINTAINERS
-----------

Current maintainers:
 * Piers Shepherd (PiersShepherd) - https://www.drupal.org/u/piersshepherd
 * Joël Pittet (joelpittet) - https://www.drupal.org/u/joelpittet
